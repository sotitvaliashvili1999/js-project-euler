const numtoTwenty = [
  undefined,
  "one".length,
  "two".length,
  "three".length,
  "four".length,
  "five".length,
  "six".length,
  "seven".length,
  "eight".length,
  "nine".length,
  "ten".length,
  "eleven".length,
  "twelve".length,
  "thirteen".length,
  "fourteen".length,
  "fifteen".length,
  "sixteen".length,
  "seventeen".length,
  "eighteen".length,
  "nineteen".length,
];
const decimal = [
  undefined,
  undefined,
  "twenty".length,
  "thirty".length,
  "forty".length,
  "fifty".length,
  "sixty".length,
  "seventy".length,
  "eighty".length,
  "ninety".length,
];
const hundred = 7;
const and = 3;

const letterCounter = num => {
  let letterQuantity = 0;
  num = String(num);
  if (num.length == 3) {
    if (num[1].concat(num[2]) === "00")
      letterQuantity += numtoTwenty[Number(num[0])] + hundred;
    else if (num[1] !== "0" && num[1] !== "1" && num[2] === "0")
      letterQuantity +=
        numtoTwenty[Number(num[0])] + hundred + and + decimal[Number(num[1])];
    else if (num[1] == "0" && num[2] !== "0")
      letterQuantity +=
        numtoTwenty[Number(num[0])] +
        hundred +
        and +
        numtoTwenty[Number(num[2])];
    else if (num[1] === "1")
      letterQuantity +=
        numtoTwenty[Number(num[0])] +
        hundred +
        and +
        numtoTwenty[Number(num[1].concat(num[2]))];
    else
      letterQuantity +=
        numtoTwenty[Number(num[0])] +
        hundred +
        and +
        decimal[Number(num[1])] +
        numtoTwenty[Number(num[2])];
  } else if (num.length == 2) {
    if (num[1] === "0" && num[0] !== "1") {
      letterQuantity += decimal[Number(num[0])];
    } else if (num[0] >= "2" && num[1] !== "0") {
      letterQuantity += decimal[Number(num[0])] + numtoTwenty[Number(num[1])];
    } else if (num[0] === "1") {
      letterQuantity += numtoTwenty[Number(num)];
    }
  } else letterQuantity += numtoTwenty[Number(num)];

  return letterQuantity;
};

let sum = 0;
for (let i = 1; i < 1000; i++) {
  sum += letterCounter(i);
}
sum += 11;

console.log(sum);
